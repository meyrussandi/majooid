import 'package:get/get.dart';
import 'package:majooid/app/data/models/favorite_model.dart';
import 'package:majooid/app/data/repository/my_database.dart';
import 'package:majooid/app/routes/app_pages.dart';

class FavoriteController extends GetxController{
  RxList<Favorite> favorites = <Favorite>[].obs;
  RxBool isLoading = false.obs;


  getAllFavorite()async{
    isLoading.value = true;
    try{
      favorites.value = await MyDatabase.instance.readAllFavorite();
      isLoading.value = false;
    }catch(e){
      isLoading.value = false;
      favorites.value = [];
    }

  }

  addFavorite(Favorite favorite)async{
    isLoading.value = true;
    try{
      await MyDatabase.instance.insertFavorite(favorite);
      favorites.refresh();
      isLoading.value = false;
      Get.offAllNamed(Routes.HOME);
    }catch(e){
      isLoading.value = false;
    }
  }

  deleteFavorite(int id)async{
    isLoading.value = true;
    try{
      await MyDatabase.instance.deleteFavorite(id);
      isLoading.value = false;
      Get.offAllNamed(Routes.HOME);
    }catch(e){
      isLoading.value = false;
    }
  }
}