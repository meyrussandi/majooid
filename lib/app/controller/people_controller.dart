import 'package:get/get.dart';
import 'package:majooid/app/data/models/people_model.dart';
import 'package:majooid/app/data/repository/my_database.dart';

class PeopleController extends GetxController{
  RxList<People> people = <People>[].obs;
  RxList<People> searchPeople = <People>[].obs;
  RxBool isGrid = false.obs;
  RxBool isList = true.obs;
  RxBool isLoading = false.obs;
  RxBool isSearch= false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  getPeople()async{
    isLoading.value = true;
    isSearch.value = false;
    try{
      people.value = await MyDatabase.instance.readAllPeople();
      isLoading.value = false;
    }catch(e){
      isLoading.value = false;
      people.value = [];
    }

  }

  buildSearchList(String keyword) {
    searchPeople.value = [];

    for (int i = 0; i < people.value.length; i++) {
      String name = people.value[i].name;
      if (name.toLowerCase().contains(keyword.toLowerCase())) {
        searchPeople.add(people.value[i]);
      }
    }

    print(searchPeople.value);
    print(isSearch.value);
  }

}