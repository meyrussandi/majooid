import 'package:flutter/material.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/core/values/mycolors.dart';

class BorderStyles {
  static Border borderGrey =
  Border.all(color: Colors.grey.withOpacity(0.4), width: 1.5);
  static Border borderPrimary =
  Border.all(color: MyColors.mainColor, width: 1.5);

  static OutlineInputBorder enableTextField = OutlineInputBorder(
    borderSide:
    const BorderSide(color: MyColors.mainColor, width: Strokes.xthin),
    borderRadius: Corners.lgBorder,
  );

  static OutlineInputBorder focusTextField = OutlineInputBorder(
    borderSide:
    const BorderSide(color: MyColors.mainColor, width: Strokes.med),
    borderRadius: Corners.lgBorder,
  );

  static OutlineInputBorder disableTextField = OutlineInputBorder(
    borderSide:
    const BorderSide(color: MyColors.mainColor, width: Strokes.xthin),
    borderRadius: Corners.lgBorder,
  );

  static OutlineInputBorder errorTextField = OutlineInputBorder(
    borderSide:
    const BorderSide(color: MyColors.mainColor, width: Strokes.xthin),
    borderRadius: Corners.lgBorder,
  );
}

InputDecoration inputDecoration(
    {required String hintText, Widget? prefixIcon, Widget? suffixIcon}) {
  return InputDecoration(
      isDense: true,
      filled: true,
      fillColor: Colors.white,
      contentPadding:
      EdgeInsets.symmetric(horizontal: Insets.med, vertical: Insets.med),
      hintText: hintText,
      border: BorderStyles.enableTextField,
      focusedBorder: BorderStyles.focusTextField,
      enabledBorder: BorderStyles.enableTextField,
      errorBorder: BorderStyles.errorTextField,
      disabledBorder: BorderStyles.disableTextField,
      errorMaxLines: 5,
      prefixIcon: prefixIcon,
      prefixIconConstraints:
      BoxConstraints(minHeight: Sizes.lg, minWidth: Sizes.lg),
      suffixIconConstraints:
      BoxConstraints(minHeight: Sizes.lg, minWidth: Sizes.lg),
      suffixIcon: suffixIcon,
      hintStyle: MyTextTheme.body1.copyWith(color: MyColors.mainColor));
}

class Shadows {
  static List<BoxShadow> get universal => [
    BoxShadow(
        color: const Color(0xff333333).withOpacity(.13),
        spreadRadius: 0,
        blurRadius: 5,
        offset: const Offset(0, 5)),
  ];
  static List<BoxShadow> get small => [
    BoxShadow(
        color: const Color(0xff333333).withOpacity(.15),
        spreadRadius: 0,
        blurRadius: 3,
        offset: const Offset(0, 1)),
  ];
  static List<BoxShadow> get none => [
    BoxShadow(
        color: MyColors.bodyColor.shade50,
        spreadRadius: 0,
        blurRadius: 0,
        offset: const Offset(0, 0)),
  ];

  static List<BoxShadow> get shadowsUp => [
    BoxShadow(
        color: const Color(0xff333333).withOpacity(.15),
        spreadRadius: 1,
        blurRadius: 3,
        offset: const Offset(-1, 0)),
  ];
}


