import 'package:majooid/app/data/models/people_model.dart';
import 'package:majooid/app/data/models/user_model.dart';

class Favorite {
  int? id;
  int userId;
  int peopleId;

  Favorite({this.id, required this.userId,required this.peopleId});

  Favorite copyWith({
    int? id,
    int? userId,
    int? peopleId,
  }) =>
      Favorite(
        id: id ?? this.id,
        userId: userId ?? this.userId,
        peopleId: peopleId ?? this.peopleId,
      );

  factory Favorite.fromJson(Map<String, dynamic> json) => Favorite(
    id: json["_id"],
    userId: json["user_id"],
    peopleId: json["people_id"],
  );

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['user_id'] = userId;
    data['people_id'] = peopleId;
    return data;
  }

  @override
  String toString() {
    return 'id: $id, userid: $userId, peopleid: $peopleId,';
  }
}
