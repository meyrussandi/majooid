import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:majooid/app/data/models/people_model.dart';
import 'package:majooid/app/data/repository/my_database.dart';

class PeopleProvider{

  static Future<List> getPeople()async{
    try{
      final response = await http.get(Uri.parse('https://swapi.dev/api/people/'));

      print(response.body);
      if(response.statusCode == 200){
        var res = jsonDecode(response.body) as Map;
        List result = res['results'];
        for (var element in result) {
          People people = People.fromJson(element);
          await MyDatabase.instance.insertPeople(people);
        }
        return [];
      }else{
        return List.empty();
      }
    }catch(e){
      return List.empty();
    }
  }
}