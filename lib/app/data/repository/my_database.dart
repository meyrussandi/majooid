import 'package:majooid/app/data/models/favorite_model.dart';
import 'package:majooid/app/data/models/people_model.dart';
import 'package:majooid/app/data/models/user_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase {
  static final MyDatabase instance = MyDatabase._init();

  static Database? _database;

  MyDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDb('majoo_database.db');
    return _database!;
  }

  Future<Database> _initDb(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path,
        version: 1,
        onCreate: _createDB,
        onUpgrade: _upgradeDB,
        onConfigure: _onConfigure);
  }

  Future _createDB(Database db, int version) async {
    await db.execute('''
    CREATE TABLE people (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    height TEXT NOT NULL,
    mass TEXT NOT NULL,
    hair_color TEXT NOT NULL,
    skin_color TEXT NOT NULL,
    eye_color TEXT NOT NULL,
    birth_year TEXT NOT NULL,
    gender TEXT NOT NULL,
    homeworld TEXT NOT NULL
    )
    ''');

    await db.execute('''
    CREATE TABLE users (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL
    )
    ''');

    await db.execute('''
    CREATE TABLE favorite (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    people_id INTEGER NOT NULL
    )
    ''');
  }

  Future _upgradeDB(Database db, int version, int newVersion) async {
    if (version == 1) {
      db.execute('DROP TABLE IF EXISTS people');
      await db.execute('''
    CREATE TABLE people (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    height TEXT NOT NULL,
    mass TEXT NOT NULL,
    hair_color TEXT NOT NULL,
    skin_color TEXT NOT NULL,
    eye_color TEXT NOT NULL,
    birth_year TEXT NOT NULL,
    gender TEXT NOT NULL,
    homeworld TEXT NOT NULL,
    )
    ''');
    }
  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }

  Future<User> createUser(User user) async {
    final db = await instance.database;

    final id = await db.insert('users', user.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return user.copyWith(id: id);
  }

  Future<People> insertPeople(People people) async {
    final db = await instance.database;

    final data = await db.query('people',
        columns: ['name', 'height', 'mass', 'hair_color'],
        where: 'name = ?',
        whereArgs: [people.name]);

    if (data.isEmpty) {
      final id = await db.insert('people', people.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace);
      return people.copyWith(id: id);
    } else {
      throw Exception('ID sudah ada');
    }
  }

  Future insertFavorite(Favorite favorite) async {
    final db = await instance.database;

    final data = await db.query('favorite',
        columns: [
          'user_id',
          'people_id',
        ],
        where: 'people_id = ?',
        whereArgs: [favorite.peopleId]);

    if (data.isEmpty) {
      final id = await db.insert('favorite', favorite.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace);
      return favorite.copyWith(id: id);
    } else {
      throw Exception('ID sudah ada');
    }
  }

  Future<int> deleteFavorite(int id) async {
    final db = await instance.database;

    return db.delete('favorite', where: 'people_id = ?', whereArgs: [id]);
  }

  Future<Favorite>? readFavorite(int id) async {
    final db = await instance.database;

    final data = await db.query('favorite',
        columns: ['_id', 'user_id', 'people_id'],
        where: 'people_id = ?',
        whereArgs: [id]);

    if (data.isNotEmpty) {
      return Favorite.fromJson(data.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<List<Favorite>> readAllFavorite() async {
    final db = await instance.database;

    final result = await db.query('favorite');

    return result.map((json) {
      return Favorite.fromJson(json);
    }).toList();
  }

  Future<User> readUser(int id) async {
    final db = await instance.database;

    final data = await db.query('users',
        columns: ['name', 'email', 'password'],
        where: '_id = ?',
        whereArgs: [id]);

    if (data.isNotEmpty) {
      return User.fromJson(data.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<People> readPeople(int id) async {
    final db = await instance.database;

    final data = await db.query('people',
        columns: ['_id','name', 'height', 'mass', 'hair_color', 'skin_color', 'eye_color','birth_year','gender','homeworld'],
        where: '_id = ?',
        whereArgs: [id]);

    if (data.isNotEmpty) {
      return People.fromJson(data.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<User>? loginUser(String email, String password) async {
    final db = await instance.database;

    final maps = await db.query('users',
        columns: ['_id', 'name', 'email', 'password'],
        where: 'email = ? AND password = ?',
        whereArgs: [email, password]);

    if (maps.isNotEmpty) {
      return User.fromJson(maps.first);
    } else {
      throw 'User not found';
    }
  }

  Future<List<User>> readAllUser() async {
    final db = await instance.database;

    final result = await db.query('users', orderBy: 'name ASC');

    return result.map((json) => User.fromJson(json)).toList();
  }

  Future<List<People>> readAllPeople() async {
    final db = await instance.database;

    final result = await db.query('people');

    return result.map((json) {
      return People.fromJson(json);
    }).toList();
  }

  Future<int> updateUser(User User) async {
    final db = await instance.database;

    return db
        .update('users', User.toJson(), where: '_id = ?', whereArgs: [User.id]);
  }

  Future<int> updatePeople(People people) async {
    final db = await instance.database;

    return db.update('people', people.toJson(),
        where: '_id = ?', whereArgs: [people.id]);
  }

  Future<int> deleteUser(int id) async {
    final db = await instance.database;

    return db.delete('users', where: '_id = ?', whereArgs: [id]);
  }

  Future<int> deletePeople(int id) async {
    final db = await instance.database;

    return db.delete('people', where: '_id = ?', whereArgs: [id]);
  }
}
