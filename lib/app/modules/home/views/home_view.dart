import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:majooid/app/modules/home/views/tabs/beranda.dart';
import 'package:majooid/app/modules/home/views/tabs/favorite.dart';
import 'package:majooid/app/modules/home/views/tabs/profile.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Majoo'),
        centerTitle: true,
      ),
      body: PageView(
        controller: controller.pageController,
        onPageChanged: (pageIndex){
          controller.tab.value = pageIndex;
        },
        children: const [
          Beranda(),
          Favorite(),
          Profile()
        ],
      ),
      bottomNavigationBar: Obx(() {
          return BottomNavigationBar(
            currentIndex: controller.tab.value,
            onTap: (tab){
              controller.tab.value = tab;
              controller.pageController.jumpToPage(tab);
            },
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Beranda'),
              BottomNavigationBarItem(icon: Icon(Icons.favorite), label: 'Favorite'),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
            ],
          );
        }
      ),
    );
  }
}
