import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:majooid/app/controller/people_controller.dart';
import 'package:majooid/app/core/theme/border_theme.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/routes/app_pages.dart';

class Beranda extends StatefulWidget {
  const Beranda({Key? key}) : super(key: key);

  @override
  State<Beranda> createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  final PeopleController _peopleController = Get.find<PeopleController>();

  @override
  void initState() {
    _getPeople();
    super.initState();
  }

  Future _getPeople()async{
    await     _peopleController.getPeople();

  }

  @override
  Widget build(BuildContext context) {
    return  Container(
        width: Get.width,
        decoration: const BoxDecoration(gradient: MyColors.gradient1),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            verticalSpace(20.h),
            TextFormField(
                style: MyTextTheme.body1,
                onChanged: (v){
                  if(v.isNotEmpty){
                    _peopleController.isSearch.value = true;
                    _peopleController.buildSearchList(v);
                  }else{
                    _peopleController.isSearch.value = false;
                  }
                },
                decoration: InputDecoration(
                  hintText: "Cari Nama",
                  filled: true,
                    fillColor: Colors.white,
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    border: BorderStyles.enableTextField
                )
            ),
            verticalSpace(10.h),
            Row(
              children: [
                Expanded(
                  child: Obx(() => InkWell(
                        onTap: () {
                          _peopleController.isGrid.value = true;
                          _peopleController.isList.value = false;
                        },
                        child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: _peopleController.isGrid.value
                                    ? MyColors.mainColor
                                    : MyColors.bodyColor.shade300),
                            child: Text('GRID')),
                      )),
                ),
                horizontalSpace(10),
                Expanded(
                  child: Obx(() => InkWell(
                        onTap: () {
                          _peopleController.isGrid.value = false;
                          _peopleController.isList.value = true;
                        },
                        child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: _peopleController.isList.value
                                    ? MyColors.mainColor
                                    : MyColors.bodyColor.shade300),
                            child: Text('LIST')),
                      )),
                ),
              ],
            ),
             Expanded(
                child: SingleChildScrollView(
                  child: Obx(
                        ()=> _peopleController.isSearch.value?Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      verticalSpace(20.h),
                      if (_peopleController.isList.value)
                        ..._peopleController.searchPeople.value
                            .map((e) => Container(
                            decoration: BoxDecoration(
                                borderRadius: Corners.lgBorder,
                                color: MyColors.bodyColor.shade50,
                                boxShadow: Shadows.universal),
                            margin: EdgeInsets.only(bottom: 10),
                            child: ListTile(
                              trailing: const Icon(Icons.arrow_forward_ios),
                              onTap: () =>
                                  Get.toNamed(Routes.UPATE, arguments: e),
                              title: Text(
                                e.name,
                                style: MyTextTheme.body1,
                              ),
                              subtitle: Text(
                                e.gender,
                                style: MyTextTheme.body2,
                              ),
                            )))
                            .toList(),
                      if (_peopleController.isGrid.value)
                        Wrap(
                          spacing: 10,
                          runSpacing: 10,
                          children: [
                            ..._peopleController.searchPeople.value
                                .map((e) => InkWell(
                              onTap: () =>
                                  Get.toNamed(Routes.UPATE, arguments: e),
                              child: Container(
                                width: (Get.width - 10 - 20 - 20) / 2,
                                height: 100,
                                decoration: BoxDecoration(
                                    borderRadius: Corners.lgBorder,
                                    color: MyColors.bodyColor.shade50,
                                    boxShadow: Shadows.universal),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      e.name,
                                      style: MyTextTheme.body1,
                                    ),
                                    Text(
                                      e.gender,
                                      style: MyTextTheme.body2.copyWith(
                                          color:
                                          MyColors.bodyColor.shade500),
                                    ),
                                  ],
                                ),
                              ),
                            ))
                                .toList(),
                          ],
                        ),
                      verticalSpace(20.h),
                    ],
                  ):Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            verticalSpace(20.h),
                            if (_peopleController.isList.value)
                              ..._peopleController.people.value
                                  .map((e) => Container(
                                  decoration: BoxDecoration(
                                      borderRadius: Corners.lgBorder,
                                      color: MyColors.bodyColor.shade50,
                                      boxShadow: Shadows.universal),
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: ListTile(
                                    trailing: const Icon(Icons.arrow_forward_ios),
                                    onTap: () =>
                                        Get.toNamed(Routes.UPATE, arguments: e),
                                    title: Text(
                                      e.name,
                                      style: MyTextTheme.body1,
                                    ),
                                    subtitle: Text(
                                      e.gender,
                                      style: MyTextTheme.body2,
                                    ),
                                  )))
                                  .toList(),
                            if (_peopleController.isGrid.value)
                              Wrap(
                                spacing: 10,
                                runSpacing: 10,
                                children: [
                                  ..._peopleController.people.value
                                      .map((e) => InkWell(
                                    onTap: () =>
                                        Get.toNamed(Routes.UPATE, arguments: e),
                                    child: Container(
                                      width: (Get.width - 10 - 20 - 20) / 2,
                                      height: 100,
                                      decoration: BoxDecoration(
                                          borderRadius: Corners.lgBorder,
                                          color: MyColors.bodyColor.shade50,
                                          boxShadow: Shadows.universal),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            e.name,
                                            style: MyTextTheme.body1,
                                          ),
                                          Text(
                                            e.gender,
                                            style: MyTextTheme.body2.copyWith(
                                                color:
                                                MyColors.bodyColor.shade500),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ))
                                      .toList(),
                                ],
                              ),
                            verticalSpace(20.h),
                          ],
                        ),
                ),
              )
            ),
          ],
        ),
      );
  }
}
