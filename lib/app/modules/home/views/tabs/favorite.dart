import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:majooid/app/controller/favorite_controller.dart';
import 'package:majooid/app/controller/people_controller.dart';
import 'package:majooid/app/core/theme/border_theme.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/data/models/people_model.dart';
import 'package:majooid/app/data/repository/my_database.dart';
import 'package:majooid/app/routes/app_pages.dart';

class Favorite extends StatefulWidget {
  const Favorite({Key? key}) : super(key: key);

  @override
  State<Favorite> createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  final FavoriteController _favoriteController = Get.find<FavoriteController>();

  @override
  void initState() {
    _getPeople();
    super.initState();
  }

  Future _getPeople()async{
    await     _favoriteController.getAllFavorite();

  }

  @override
  Widget build(BuildContext context) {
    return  Container(
        width: Get.width,
        decoration: const BoxDecoration(gradient: MyColors.gradient1),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            verticalSpace(20.h),
             Expanded(
                child: SingleChildScrollView(
                  child: Obx(
                        ()=>  _favoriteController.favorites.value.isEmpty?
                            Center(
                              child: Text('Kosong', style: MyTextTheme.body1,),
                            ):Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            verticalSpace(20.h),
                            ..._favoriteController.favorites.value
                                .map((e) => Container(
                                decoration: BoxDecoration(
                                    borderRadius: Corners.lgBorder,
                                    color: MyColors.bodyColor.shade50,
                                    boxShadow: Shadows.universal),
                                margin: EdgeInsets.only(bottom: 10),
                                child: FutureBuilder<People>(
                                    future: MyDatabase.instance.readPeople(e.peopleId),
                                  builder: (context, snapshot) {
                                      if(snapshot.connectionState == ConnectionState.waiting){
                                        return SizedBox();
                                      }
                                      if(!snapshot.hasData){
                                        return SizedBox();
                                      }

                                    return ListTile(
                                      trailing: const Icon(Icons.arrow_forward_ios),
                                      onTap: () =>
                                          Get.toNamed(Routes.UPATE, arguments: snapshot.data),
                                      title: Text(
                                        snapshot.data!.name.toString(),
                                        style: MyTextTheme.body1,
                                      ),
                                      subtitle: Text(
                                        snapshot.data!.gender.toString(),
                                        style: MyTextTheme.body2,
                                      ),
                                    );
                                  }
                                )))
                                .toList(),

                            verticalSpace(20.h),
                          ],
                        ),
              )
            ),
             )
          ],
        ),
      );
  }
}
