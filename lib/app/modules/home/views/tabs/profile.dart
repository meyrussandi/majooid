import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:majooid/app/controller/user_controller.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/routes/app_pages.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final UserController userController = Get.find<UserController>();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(gradient: MyColors.gradient1),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Center(
        child: Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: Corners.lgBorder
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(radius: 50,),
              Text(userController.user.last.name, style: MyTextTheme.heading3),
              Text(userController.user.last.email, style: MyTextTheme.heading4.copyWith(color: MyColors.bodyColor.shade300)),
              ElevatedButton(onPressed: (){
                userController.user.value = [];
                Get.offAllNamed(Routes.LOGIN);
              }, child: Text('Keluar'))
            ],
          ),
        ),
      ),
    );
  }
}
