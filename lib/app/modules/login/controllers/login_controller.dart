import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:majooid/app/controller/user_controller.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/data/providers/people_provider.dart';
import 'package:majooid/app/data/repository/my_database.dart';
import 'package:majooid/app/routes/app_pages.dart';

class LoginController extends GetxController {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  RxBool showPassword = false.obs;
  RxBool isLoading = false.obs;
  UserController userController = Get.find();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void login() async {
    isLoading.value = true;
    try {
      var data =
          await MyDatabase.instance.loginUser(email.text, password.text);

          if(data != null){
            await getPeople();
            isLoading.value = false;
            userController.user.add(data);
            Get.offAllNamed(Routes.HOME);
          }
      isLoading.value = false;

    } catch (e) {
      isLoading.value = false;
      Get.snackbar('Error', e.toString(), backgroundColor: MyColors.mainColor, colorText: Colors.white);
    }
    }


  getPeople()async{
    await PeopleProvider.getPeople();
  }

  @override
  void onClose() {
    email.dispose();
    password.dispose();
  }
}