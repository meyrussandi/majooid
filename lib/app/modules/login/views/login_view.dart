import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/routes/app_pages.dart';
import 'package:majooid/app/widgets/loading_overlay.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Obx(()=>LoadingOverlay(
      isLoading: controller.isLoading.value,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: Get.height,
            decoration: const BoxDecoration(
              gradient: MyColors.gradient1
            ),
            child: Column(
              children: [
                const Spacer(),
                Align(
                  alignment: Alignment.topCenter,
                  child: Center(
                    child: CircleAvatar(
                      radius: 50.w,
                    ),
                  ),
                ),
                const Spacer(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      padding: EdgeInsets.all(Insets.lg),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.vertical(top: Corners.xxlRadius)),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Selamat datang kembali !!!",
                                      style: MyTextTheme.body1,
                                    ),
                                    Text(
                                      "Masuk",
                                      style: MyTextTheme.heading4,
                                    ),
                                  ],
                                ),

                              ],
                            ),
                            verticalSpace(Insets.lg),
                            TextField(
                              controller: controller.email,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                label: Text(
                                  "Email",
                                  style: MyTextTheme.small1
                                      .copyWith(color: MyColors.mainColor),
                                ),
                                hintText: "Masukan Email",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                              ),
                            ),
                            verticalSpace(Insets.med),
                            TextField(
                              controller: controller.password,
                              obscureText: !controller.showPassword.value,
                              decoration: InputDecoration(
                                suffixIcon: Obx(()=>IconButton(onPressed: (){
                                  controller.showPassword.value = !controller.showPassword.value;
                                }, icon: controller.showPassword.value?Icon(Icons.visibility_off):Icon(Icons.visibility)),
                                ),
                                label: Text(
                                  "kata sandi",
                                  style: MyTextTheme.small1
                                      .copyWith(color: MyColors.mainColor),
                                ),
                                hintText: "Masukan kata sandi",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                              ),
                            ),
                            verticalSpace(Insets.xl),

                            ElevatedButton(
                              onPressed: (){
                                controller.login();
                                FocusScope.of(context).unfocus();
                              },
                              child: Text(
                                "Masuk",
                                style: MyTextTheme.body1,
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: MyColors.mainColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: Corners.lgBorder,
                                  ),
                                  fixedSize: Size(Get.width, Sizes.xl)),
                            ),
                            verticalSpace(Insets.xl),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text("Belum punya akun? "),
                                InkWell(
                                  onTap: ()=>Get.toNamed(Routes.REGISTER),
                                  child: Text(
                                    "Daftar",
                                    style: MyTextTheme.body1
                                        .copyWith(color: Colors.red),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    ),
    );
  }
}