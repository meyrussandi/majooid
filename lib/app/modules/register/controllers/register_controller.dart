import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/data/models/user_model.dart';
import 'package:majooid/app/data/repository/my_database.dart';

class RegisterController extends GetxController {
  TextEditingController nama = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController konfirmasiPassword = TextEditingController();
  RxBool isLoading = false.obs;
  RxBool showPassword = false.obs;


  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void confirmRegister() {
    print(nama.text.isEmpty);
    if (nama.text.isEmpty) {
      Get.snackbar("Info", "Nama tidak boleh kosong",
          backgroundColor: MyColors.mainColor);
    } else if (email.text.isEmpty) {
      Get.snackbar("Info", "email tidak boleh kosong",
          backgroundColor: MyColors.mainColor);
    } else if (password.text.isEmpty) {
      Get.snackbar("Info", "password tidak boleh kosong",
          backgroundColor: MyColors.mainColor);
    } else {
      register();
    }
  }

  register()async{
    isLoading.value = true;

    try{
      await MyDatabase.instance.createUser(User(name: nama.text, email: email.text, password: password.text));
      var list = await MyDatabase.instance.readAllUser();

      print('list user');
      print(list);
      isLoading.value = false;
      Get.back();
    } catch (e) {

      isLoading.value = false;

      Get.snackbar('Error', e.toString(), backgroundColor: MyColors.mainColor, colorText: Colors.white);

    }


  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
