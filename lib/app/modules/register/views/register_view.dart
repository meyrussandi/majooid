import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/routes/app_pages.dart';
import 'package:majooid/app/widgets/loading_overlay.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    return Obx(()=>LoadingOverlay(
      isLoading: controller.isLoading.value,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: Get.height,
            decoration: const BoxDecoration(
                gradient: MyColors.gradient1
            ),
            child: Column(
              children: [
                const Spacer(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      padding: EdgeInsets.all(Insets.lg),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.vertical(top: Corners.xxlRadius)),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Silahkan isi data anda !!!",
                                      style: MyTextTheme.body1,
                                    ),
                                    Text(
                                      "Daftar",
                                      style: MyTextTheme.heading4,
                                    ),
                                  ],
                                ),
                                IconButton(
                                    onPressed: ()=>Get.back(),
                                    icon: const Icon(
                                      Icons.close,
                                      color: Colors.red,
                                    ))
                              ],
                            ),
                            verticalSpace(Insets.lg),
                            TextField(
                              controller: controller.nama,
                              decoration: InputDecoration(
                                label: Text(
                                  "Nama",
                                  style: MyTextTheme.small1
                                      .copyWith(color: MyColors.mainColor),
                                ),
                                hintText: "Masukan Nama",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                              ),
                            ),
                            verticalSpace(Insets.med),
                            TextField(
                              controller: controller.email,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                label: Text(
                                  "Email",
                                  style: MyTextTheme.small1
                                      .copyWith(color: MyColors.mainColor),
                                ),
                                hintText: "Masukan Email",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                              ),
                            ),
                            verticalSpace(Insets.med),
                            TextField(
                              controller: controller.password,
                              obscureText: !controller.showPassword.value,
                              decoration: InputDecoration(
                                suffixIcon: Obx(()=>IconButton(onPressed: (){
                                  controller.showPassword.value = !controller.showPassword.value;
                                }, icon: controller.showPassword.value?Icon(Icons.visibility_off):Icon(Icons.visibility)),
                                ),
                                label: Text(
                                  "kata sandi",
                                  style: MyTextTheme.small1
                                      .copyWith(color: MyColors.mainColor),
                                ),
                                hintText: "Masukan kata sandi",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(Corners.xl),
                                ),
                              ),
                            ),
                            verticalSpace(Insets.xl),
                            ElevatedButton(
                              onPressed: (){
                                controller.confirmRegister();
                                FocusScope.of(context).unfocus();
                              },
                              child: Text(
                                "Daftar",
                                style: MyTextTheme.body1,
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: MyColors.mainColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: Corners.lgBorder,
                                  ),
                                  fixedSize: Size(Get.width, Sizes.xl)),
                            ),
                            verticalSpace(Insets.xl),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text("Sudah punya akun? "),
                                InkWell(
                                  onTap: ()=>Get.offNamed(Routes.LOGIN),
                                  child: Text(
                                    "Login",
                                    style: MyTextTheme.body1
                                        .copyWith(color: Colors.red),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    ),
    );
  }
}
