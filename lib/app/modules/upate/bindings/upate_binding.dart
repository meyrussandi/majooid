import 'package:get/get.dart';

import '../controllers/upate_controller.dart';

class UpateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UpdateController>(
      () => UpdateController(),
    );
  }
}
