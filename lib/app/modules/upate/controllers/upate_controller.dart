import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:majooid/app/controller/favorite_controller.dart';
import 'package:majooid/app/controller/people_controller.dart';
import 'package:majooid/app/controller/user_controller.dart';
import 'package:majooid/app/core/values/mycolors.dart';
import 'package:majooid/app/data/models/favorite_model.dart';
import 'package:majooid/app/data/models/people_model.dart';
import 'package:majooid/app/data/repository/my_database.dart';
import 'package:majooid/app/routes/app_pages.dart';

class UpdateController extends GetxController {
  People people = Get.arguments;
  UserController userController = Get.find();
  TextEditingController nama = TextEditingController();
  TextEditingController height = TextEditingController();
  TextEditingController mass = TextEditingController();
  TextEditingController hairColor = TextEditingController();
  TextEditingController skinColor = TextEditingController();
  TextEditingController eyeColor = TextEditingController();
  RxBool isLoading = false.obs;
  RxBool isFav = false.obs;

  @override
  void onInit() {
    readFavorite(people.id!);
    nama = TextEditingController(text: people.name);
    height = TextEditingController(text: people.height);
    mass = TextEditingController(text: people.mass);
    hairColor = TextEditingController(text: people.hairColor);
    skinColor = TextEditingController(text: people.skinColor);
    eyeColor = TextEditingController(text: people.eyeColor);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  simpanData() async {
    Get.defaultDialog(
        title: "info",
        middleText: "Yakin Simpan data?",
        onConfirm: () async {
          try {
            isLoading.value = true;
            await MyDatabase.instance.updatePeople(people.copyWith(
              name: nama.text,
              height: height.text,
              mass: mass.text,
              hairColor: hairColor.text,
              skinColor: skinColor.text,
              eyeColor: eyeColor.text,
            ));

            Get.find<PeopleController>().people.refresh();

            Get.back();
            isLoading.value = false;
            Get.offAllNamed(Routes.HOME);
          } catch (e) {
            isLoading.value = false;

            Get.snackbar('Error', e.toString(),
                backgroundColor: MyColors.mainColor, colorText: Colors.white);
          }
        },
        onCancel: () {
        });
  }

  hapusData() async {
    Get.defaultDialog(
        title: "info",
        middleText: "Yakin Hapus data?",
        onConfirm: () async {
          try {
            isLoading.value = true;
            await MyDatabase.instance.deletePeople(people.id!);

            Get.find<PeopleController>().people.refresh();

            Get.back();
            isLoading.value = false;
            Get.offAllNamed(Routes.HOME);
          } catch (e) {
            isLoading.value = false;

            Get.snackbar('Error', e.toString(),
                backgroundColor: MyColors.mainColor, colorText: Colors.white);
          }
        },
        onCancel: () {
        });
  }

  readFavorite(int id)async{
    isLoading.value = true;
    try{
      var data = await MyDatabase.instance.readFavorite(id);
      print('data');
      print(data);
      isFav.value = true;
      isLoading.value = false;
    }catch(e){
      isFav.value = false;
      isLoading.value = false;
    }
  }

  addFav() {
    Get.find<FavoriteController>()
        .addFavorite(Favorite(userId: userController.user.last.id!, peopleId: people.id!));
  }

  deleteFav() {
    Get.find<FavoriteController>()
        .deleteFavorite(people.id!);
  }

  @override
  void onClose() {
    nama.dispose();
    height.dispose();
    mass.dispose();
    hairColor.dispose();
    skinColor.dispose();
    eyeColor.dispose();
  }
}
