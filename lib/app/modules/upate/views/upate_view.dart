import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:majooid/app/controller/favorite_controller.dart';
import 'package:majooid/app/core/theme/border_theme.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/constants.dart';
import 'package:majooid/app/data/models/favorite_model.dart';

import '../controllers/upate_controller.dart';

class UpateView extends GetView<UpdateController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('UpateView'),
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () {
                  controller.hapusData();
                },
                icon: const Icon(Icons.delete_forever))
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                TextFormField(
                    controller: controller.nama,
                    style: MyTextTheme.body1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        label: const Text('nama'),
                        border: BorderStyles.enableTextField)),
                verticalSpace(10),
                TextFormField(
                    controller: controller.height,
                    style: MyTextTheme.body1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        label: const Text('height'),
                        border: BorderStyles.enableTextField)),
                verticalSpace(10),
                TextFormField(
                    controller: controller.mass,
                    style: MyTextTheme.body1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        label: const Text('mass'),
                        border: BorderStyles.enableTextField)),
                verticalSpace(10),
                TextFormField(
                    controller: controller.hairColor,
                    style: MyTextTheme.body1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        label: const Text('hair color'),
                        border: BorderStyles.enableTextField)),
                verticalSpace(10),
                TextFormField(
                    controller: controller.skinColor,
                    style: MyTextTheme.body1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        label: const Text('skin color'),
                        border: BorderStyles.enableTextField)),
                verticalSpace(10),
                TextFormField(
                    controller: controller.eyeColor,
                    style: MyTextTheme.body1,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        label: const Text('eye color'),
                        border: BorderStyles.enableTextField)),
                verticalSpace(20),
                ElevatedButton(
                    onPressed: () {
                      controller.simpanData();
                    },
                    child: Text(
                      'Simpan',
                      style: MyTextTheme.body1,
                    )),
                Obx(
                    ()=>controller.isFav.value?ElevatedButton(
                        onPressed: () {
                          controller.deleteFav();
                        },
                        child: Text(
                          'Hapus dari favorite',
                          style: MyTextTheme.body1,
                        )): ElevatedButton(
                      onPressed: () {
                        controller.addFav();
                      },
                      child: Text(
                        'Tambah ke favorite',
                        style: MyTextTheme.body1,
                      )),
                ),
              ],
            ),
          ),
        ));
  }
}
