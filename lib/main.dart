import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:majooid/app/controller/favorite_controller.dart';
import 'package:majooid/app/controller/people_controller.dart';
import 'package:majooid/app/controller/user_controller.dart';
import 'package:majooid/app/core/theme/text_theme.dart';
import 'package:majooid/app/core/values/mycolors.dart';

import 'app/routes/app_pages.dart';

void main() {
  // runApp(DevicePreview( builder: (context) => MyApp()));

  Get.put(PeopleController());
  Get.put(FavoriteController());
  Get.put(UserController());
  runApp(MyApp());
}

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return ScreenUtilInit(
//       designSize: const Size(375, 667),
//       builder: () =>GetMaterialApp(
//         debugShowCheckedModeBanner: false,
//         builder: DevicePreview.appBuilder,
//         title: "Application",
//         initialRoute: AppPages.INITIAL,
//         getPages: AppPages.routes,
//       ),
//     );
//   }
// }
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: () {
        return GetMaterialApp(
          theme: ThemeData(
            brightness: Brightness.light,
            primaryColor: MyColors.mainColor,
            primarySwatch: MyColors.mainColor,
            textTheme: TextTheme(
              bodyText1: MyTextTheme.body1,
              bodyText2: MyTextTheme.body2,
            ).apply(
              bodyColor: MyColors.bodyColor[900],
              displayColor: MyColors.bodyColor[900],
            ),
            fontFamily: 'Inter',
          ),

            debugShowCheckedModeBanner: false,
            builder: DevicePreview.appBuilder,
            title: "Application",
            initialRoute: AppPages.INITIAL,
            getPages: AppPages.routes,

        );
      }
    );
  }
}
